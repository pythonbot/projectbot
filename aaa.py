# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *

SLACK_TOKEN = "xoxb-683378523137-678331450771-yyhGxJAkhXj9gRiN254IZb3p"
SLACK_SIGNING_SECRET = "f662adff1d603e9809dcf0c8205d495d"


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

def _crawl_menu_keywords(text) :
    if "점심 메뉴 셰프" in text :
        url = "http://welfoodstory.azurewebsites.net/?category=2%EC%BA%A0%ED%8D%BC%EC%8A%A4-3"
        sourcecode = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(sourcecode, "html.parser")

        menu_title = []
        for data in soup.find_all("div", class_="menu-item-title"):
            menu_title.append(data.get_text().strip())

        menu_contents = []
        for data in soup.find_all("div", class_="menu-item-contents"):
            menu_contents.append(data.get_text().strip())
        
        menu_image = []
        for data in soup.find_all("img"):
            if "http://samsungwelstory.com/data/manager/recipe" in data.get('src') :
                menu_image.append(data.get('src'))

        total_menu = []
        for j in range(15,len(menu_title)-2) :
            if "셰프" in menu_title[j] :
                total_menu.append(menu_image[j-1] + "\n" + menu_title[j] + "\n" + menu_contents[j] +"\n")
        return u'\n'.join(total_menu)

    elif "점심 메뉴 테이크 아웃" in text :
        url = "http://welfoodstory.azurewebsites.net/?category=2%EC%BA%A0%ED%8D%BC%EC%8A%A4-3"
        sourcecode = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(sourcecode, "html.parser")

        menu_title = []
        for data in soup.find_all("div", class_="menu-item-title"):
            menu_title.append(data.get_text().strip())

        
        menu_contents = []
        for data in soup.find_all("div", class_="menu-item-contents"):
            menu_contents.append(data.get_text().strip())

        menu_image = []
        for data in soup.find_all("img"):
            if "http://samsungwelstory.com/data/manager/recipe" in data.get('src') :
                menu_image.append(data.get('src'))

        total_menu = []
        for j in range(12,len(menu_title)-13) :
            if "T/O" in menu_title[j] :
                total_menu.append(menu_image[j-1] + "\n" + menu_title[j] + "\n" + menu_contents[j] +"\n")
        return u'\n'.join(total_menu)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
 
    message = _crawl_menu_keywords(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text=message
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)